/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package exercisesp2;
import java.util.Random;
import JavaObjectsClasses.SavingsAccount;
import JavaObjectsClasses.Car;
import JavaObjectsClasses.RoachPopulation;
import java.lang.InterruptedException;

/**
 *
 * @author 114158
 */
public class Main {
    public static void printExerciseHeader(int chapter, int number, String description)
    {
        System.out.println();
        System.out.println("<----------------------->");
        System.out.println("Exercise P" + chapter + "." + number);
        System.out.println("(" + description + ")");
    }
    public static void waitForInput()
    {
        try{ System.out.wait(1000); }  catch(InterruptedException e) {
        System.out.print(e); }
    }
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        printExerciseHeader(1, 6, "Fraction Addition");
        int denomNums = 10;
        float denomSum = 0;
        for(float calculate = 1; calculate <= denomNums; calculate++)
        {
            denomSum += 1 / calculate;
        }
        System.out.println("Sum:" + denomSum);
        //waitForInput();
        
        printExerciseHeader(2, 7, "Random die roll from 1-6");
        Random rndNumber = new Random();
        System.out.println("Die Roll: " + (rndNumber.nextInt(5) + 1));
        
        printExerciseHeader(2, 9, "Replace 'Mississippi' with symbols");
        String inputWord = "Mississippi";
        System.out.println("Original Word: " + inputWord.toLowerCase());
        System.out.println("Altered Word: " + inputWord.toLowerCase().replace("i", "!").replace("s","$"));
        
        printExerciseHeader(3, 3, "SavingsAccount Interest");
        SavingsAccount svgAccount = new SavingsAccount(1000, 10);
        for(int addInterest = 0; addInterest < 5; addInterest++)
        { svgAccount.addInterest(); }
        System.out.println("Final Balance: " + svgAccount.getBalance());
        
        printExerciseHeader(3, 6, "Driving Cars");
        Car carHorrid = new Car(12);
        carHorrid.addGas(17);
        carHorrid.drive(117);
        System.out.println("Remaining Fuel: " + carHorrid.getGas());
        
        printExerciseHeader(3, 12, "Roach Population");
        RoachPopulation roaches = new RoachPopulation(10);
        for(int angryMotherAttackWave = 0; angryMotherAttackWave < 4; angryMotherAttackWave++)
        {
            roaches.waitingForDouble();
            roaches.spray();
            System.out.print("Attack Wave " + (angryMotherAttackWave + 1) + ": ");
            System.out.println(roaches.getPopulation());
        }
    }

}
