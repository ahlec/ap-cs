package com.deitloff.gui;
import com.deitloff.zoology.*;

public class Barnyard
{
    private java.util.ArrayList<Animal> _animals;
    private java.util.ArrayList<String> _song;
    
    public Barnyard(java.awt.Dimension farmDimension)
    {
        // Purchasing Land
        _animals = new java.util.ArrayList<Animal>();
        Interface _interface = new Interface(this);
        
        // Populating Barnyard
        _animals.add(new Cow());
        _animals.add(new Dog());
        _animals.add(new Sheep());
        _animals.add(new Chicken());
        _animals.add(new Cat());
        _animals.add(new Horse());
        _animals.add(new Bull());
        _animals.add(new Pig());
        
        // Plowing Pastures
        _interface.setLocation((farmDimension.width - _interface.getWidth()) / 2,
                (farmDimension.height - _interface.getHeight()) / 2);
        _interface.setVisible(true);
        _interface.jPanel1.setVisible(true);
        
        // Proding Livestock
        for (Animal livestock : _animals)
            _interface.addAnimalOption(livestock);
        
        // Procuring Songbook
        _song = new java.util.ArrayList<String>();
        _song.add("ol' mcdonald had a farm");
        _song.add("E-I-E-I-O");
        _song.add("and on this farm he had {v_article} {v_animal},");
        _song.add("E-I-E-I-O.");
        _song.add("with {v_article} {v_speak}, {v_speak} here,");
        _song.add("and {v_article} {v_speak}, {v_speak} there,");
        _song.add("here {v_article} {v_speak}, there {v_article} {v_speak},");
        _song.add("everywhere {v_article} {v_speak}, {v_speak}.");
        _song.add("ol' mcdonald had a farm,");
        _song.add("E-I-E-I-O.");
    }
    
    private String DetermineIndefiniteArticle(String word)
    {
        String firstChar = word.substring(0, 1).toLowerCase();
        if (firstChar.equals("a") || firstChar.equals("e") ||
                firstChar.equals("i") || firstChar.equals("o") ||
                firstChar.equals("u"))
            return "an";
        return "a";
    }
    
    public String createSongVerse(int animalIndex)
    {
        String verse = "";
        for (String line : _song)
            verse += line + "\n";
        Animal songAnimal = _animals.get(animalIndex);
        verse = verse.replaceAll("\\{v_article\\}",
                DetermineIndefiniteArticle(songAnimal.whatAmI()));
        verse = verse.replaceAll("\\{v_speak\\}", songAnimal.speak());
        verse = verse.replaceAll("\\{v_animal\\}", songAnimal.whatAmI());
        return verse + "\n";
    }
    
    public javax.swing.ImageIcon getAnimalPicture(int animalIndex)
    {
        return new javax.swing.ImageIcon(_animals.get(animalIndex).getPic());
    }
    public String getAnimalName(int animalIndex)
    {
        String _animalName = _animals.get(animalIndex).whatAmI();
        if (_animalName.isEmpty())
            return "";
        else if (_animalName.length() == 1)
            return _animalName;
        else
            return _animalName.substring(0, 1).toUpperCase() + _animalName.substring(1).toLowerCase();
    }
}
