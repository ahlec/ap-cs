/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package arrayarraylistexercises;

/**
 *
 * @author 114158
 */
public class SelfDivisor {

    /** @param number the number to be tested
		*	Precondition:  number > 0
	   *    @return true  if every decimal digit of number is a divisor of number
	   *            false otherwise */
    public static boolean isSelfDivisor(int number)
    {
        if (number <= 0)
            return false;
        String divisNumber = Integer.toString(number);
        for (int checking = 0; checking < divisNumber.length(); checking++)
        {
            int individual = Integer.parseInt(divisNumber.substring(checking, checking + 1));
            if (individual == 0)
                return false;
            if (number % individual != 0)
                return false;
        }
        return true;
    }
    
    	/** @param start starting point for values to be checked
		*	Precondition:  start > 0
		*    @param num the size of the array to be returned
		*	Precondition:  num > 0
	   *    @return an array containing the first num integers ≥ start that are self-divisors
	   */  

    public static int[] firstNumSelfDivisors(int start, int num)
    {
        if (start <= 0 || num <= 0)
            return new int[0];
        int[] selfDivisors = new int[num];
        int divisorsFound = 0;
        for (int numberCheck = start; divisorsFound < num; numberCheck++)
            if (SelfDivisor.isSelfDivisor(numberCheck))
            {
                selfDivisors[divisorsFound] = numberCheck;
                divisorsFound++;
            }
        return selfDivisors;
    }
}