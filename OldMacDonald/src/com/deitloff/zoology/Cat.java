/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.deitloff.zoology;

/**
 *
 * @author 114158
 */
public class Cat implements Mammal
{
    public String whatAmI()
    {
        return "cat";
    }
    public String speak()
    {
        return "meow";
    }
    public String getPic()
    {
        return "src/com/deitloff/zoology/pictureCat.png";
    }
}
