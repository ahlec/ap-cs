/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.deitloff.buggies;
import info.gridworld.actor.*;
import info.gridworld.grid.Grid;
import info.gridworld.grid.Location;

/**
 *
 * @author 114158
 */
public class NeighbourBug extends Bug
{
    public int countNeighbours()
    {
        Grid<Actor> _grid = getGrid();
        int _neighbors = 0;
        for (int direction = 0; direction < 360; direction += 45)
        {
            Location newLoc = getLocation().getAdjacentLocation(direction);
            if (_grid.isValid(newLoc))
                if (_grid.get(newLoc) instanceof Bug)
                    _neighbors++;
        }
        return _neighbors;
    }
}
