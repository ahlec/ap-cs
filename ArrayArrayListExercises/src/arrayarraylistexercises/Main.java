/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package arrayarraylistexercises;
import arrayarraylistexercises.Square;
import arrayarraylistexercises.SelfDivisor;

/**
 *
 * @author 114158
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        int[] numbers = {1,4,9,16,9,7,4,9,11};
        int numberResult = 0;
        for (int number = 0; number < numbers.length; number++)
        {
            if (number % 2 == 0)
                numberResult += numbers[number];
            else
                numberResult -= numbers[number];
        }
        System.out.println("(P8.10) Result: " + numberResult);
     
        Square mgSquare = new Square(4);
        mgSquare.add(16);
        mgSquare.add(3);
        mgSquare.add(2);
        mgSquare.add(13);
        mgSquare.add(5);
        mgSquare.add(10);
        mgSquare.add(11);
        mgSquare.add(8);
        mgSquare.add(9);
        mgSquare.add(6);
        mgSquare.add(7);
        mgSquare.add(12);
        mgSquare.add(4);
        mgSquare.add(15);
        mgSquare.add(14);
        mgSquare.add(1);
        System.out.println();
        System.out.println(mgSquare.toString());
        System.out.println("(P8.18) Is A Magic Square? " + mgSquare.isMagic());
        
        System.out.println();
        System.out.println("(Self-Divisor) 9? " + SelfDivisor.isSelfDivisor(9));
        System.out.println("(Self-Divisor) 99? " + SelfDivisor.isSelfDivisor(99));
        System.out.println("(Self-Divisor) 118? " + SelfDivisor.isSelfDivisor(118));
        System.out.print("(Self-Divisor) First 10 self divisors starting from 30? ");
        int[] selfDivisors = SelfDivisor.firstNumSelfDivisors(30, 10);
        for (int outDivisors = 0; outDivisors < 10; outDivisors++)
        {
            System.out.print(Integer.toString(selfDivisors[outDivisors]));
            if (outDivisors < 9)
                System.out.print(", ");
            else
                System.out.println();
        }
    }

}
