/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.deitloff.buggies;
import info.gridworld.grid.Grid;
import info.gridworld.grid.Location;
import info.gridworld.actor.Actor;

/**
 *
 * @author 114158
 */
public class ButtBug extends NeighbourBug
{
    @Override
    public boolean canMove() {
        Grid<Actor> _grid = getGrid();
        if (_grid == null)
            return false;
        int newDirection = 180 + getDirection();
        while (newDirection >= 360)
            newDirection -= 360;
        Location newLocation = getLocation().getAdjacentLocation(newDirection);
        if (!_grid.isValid(newLocation))
            return false;
        Actor actorInNewLoc = _grid.get(newLocation);
        return (actorInNewLoc == null) || (actorInNewLoc instanceof info.gridworld.actor.Flower);
    }

    @Override
    public void move() {
        Grid<Actor> _grid = getGrid();
        if (_grid == null)
            return;
        int newDirection = 180 + getDirection();
        while (newDirection >= 360)
            newDirection -= 360;
        Location oldLoc = getLocation();
        Location newLocation = getLocation().getAdjacentLocation(newDirection);
        if (_grid.isValid(newLocation))
            moveTo(newLocation);
        else
            removeSelfFromGrid();
        info.gridworld.actor.Flower poop = new info.gridworld.actor.Flower(getColor());
        poop.putSelfInGrid(_grid, oldLoc);
    }
    
}
