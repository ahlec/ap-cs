/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.deitloff.zoology;

public class Dog implements Mammal {
     public String getPic()
     {
         return "src/com/deitloff/zoology/pictureDog.png";
     }
     public String speak()
     {
         return "woof";
     }
     public String whatAmI()
     {
         return "dog";
     }
}
