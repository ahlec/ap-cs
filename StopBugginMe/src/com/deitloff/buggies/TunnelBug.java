/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.deitloff.buggies;
import info.gridworld.grid.Grid;
import info.gridworld.grid.Location;
import info.gridworld.actor.*;

/**
 *
 * @author 114158
 */
public class TunnelBug extends NeighbourBug
{

    @Override
    public boolean canMove() {
        Grid<Actor> _grid = getGrid();
        Location locAhead = getLocation().getAdjacentLocation(getDirection());
        if (!_grid.isValid(locAhead))
            return false;
        Actor _dirAhead = _grid.get(locAhead);
        if (_dirAhead == null || _dirAhead instanceof Flower)
            return true;
        while (_dirAhead instanceof Rock)
        {
            locAhead = locAhead.getAdjacentLocation(getDirection());
            if (!_grid.isValid(locAhead))
                return false;
            _dirAhead = _grid.get(locAhead);
        }
        if (_dirAhead == null || _dirAhead instanceof Flower)
            return true;
        return false;
    }

    @Override
    public void move() {
        Grid<Actor> _grid = getGrid();
        Flower poop = new Flower(getColor());
        if (_grid == null)
            return;
        Location oldLoc = getLocation();
        Location newLocation = getLocation().getAdjacentLocation(getDirection());
        if (!_grid.isValid(newLocation))
        {
            removeSelfFromGrid();
            poop.putSelfInGrid(_grid, oldLoc);
            return;
        }
        Actor _actorAhead = _grid.get(newLocation);
        while (_actorAhead instanceof Rock)
        {
            newLocation = newLocation.getAdjacentLocation(getDirection());
            if (!_grid.isValid(newLocation))
            {
                removeSelfFromGrid();
                poop.putSelfInGrid(_grid, oldLoc);
                return;
            }
            _actorAhead = _grid.get(newLocation);
        }
        
        if (_grid.isValid(newLocation))
            moveTo(newLocation);
        else
            removeSelfFromGrid();
        poop.putSelfInGrid(_grid, oldLoc);
    }
}
