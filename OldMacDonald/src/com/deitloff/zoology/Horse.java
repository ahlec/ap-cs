/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.deitloff.zoology;

/**
 *
 * @author 114158
 */
public class Horse implements Mammal
{
    public String getPic()
    {
        return "src/com/deitloff/zoology/pictureHorse.png";
    }
    public String whatAmI()
    {
        return "horse";
    }
    public String speak()
    {
        return "neigh";
    }
}
