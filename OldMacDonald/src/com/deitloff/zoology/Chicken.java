/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.deitloff.zoology;

/**
 *
 * @author 114158
 */
public class Chicken implements Aviary
{
    public String getPic()
    {
        return "src/com/deitloff/zoology/pictureChicken.png";
    }
    public String speak()
    {
        return "cluck";
    }
    public String whatAmI()
    {
        return "chicken";
    }
}
