/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package Dictionary;
import java.awt.Color;

/**
 *
 * @author Jacob
 */
public enum PartOfSpeech {
Noun (Color.GREEN),
Verb (Color.RED),
Adjective (Color.BLUE),
Adverb (Color.YELLOW),
Pronoun (Color.MAGENTA),
Interjection (Color.ORANGE),
Punctuation (Color.LIGHT_GRAY);
    private final Color posColor;
    PartOfSpeech(Color clr) { this.posColor = clr; }
    
    /* Returns the system color used to represent that part of speech
     * @return  the color used program-wide to represent that color
     */
    public Color getPOSColor() { return posColor; }
}
