/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package ProgrammingClasses;

import java.awt.Graphics;

/**
 *
 * @author 114158
 */
public class SimulationPanel extends javax.swing.JPanel {

    boolean simulating = false;
    int[] xPnts = new int[0];
    int[] yPnts = new int[0];
    public void addPoint(int x, int y)
    {
        int[] newXPnts = new int[xPnts.length + 1];
        int[] newYPnts = new int[yPnts.length + 1];
        System.arraycopy(xPnts, 0, newXPnts, 0, xPnts.length);
        newXPnts[newXPnts.length - 1] = x;
        System.arraycopy(yPnts, 0, newYPnts, 0, yPnts.length);
        newYPnts[newYPnts.length - 1] = y;
        xPnts = newXPnts;
        yPnts = newYPnts;
        this.repaint();
    }

    public void beginSimulation() { this.simulating = true; }
    public void endSimulation() { this.simulating = false; }
    public void clearSimulation()
    {
        xPnts = new int[0];
        yPnts = new int[0];
        simulating = false;
        drawSimulationPanel = true;
        this.repaint();
    }
    
    private boolean drawSimulationPanel = true;
    @Override
    public void paint(Graphics g) {
        //super.paint(g);
        if(drawSimulationPanel)
        {
            g.setColor(new java.awt.Color(204, 255, 255));
            g.fillRect(0, 0, this.getWidth() - 1, this.getHeight() - 1);
            g.setColor(new java.awt.Color(51, 51, 255));
            g.drawRect(0, 0, this.getWidth() - 1, this.getHeight() - 1);
            g.setColor(java.awt.Color.black);
            drawSimulationPanel = false;
        }
        g.drawPolyline(xPnts, yPnts, xPnts.length);
        g.fillOval(xPnts[xPnts.length - 1]- 2, yPnts[yPnts.length - 1] - 2, 4,4);
        if(simulating)
            g.drawString("Simulating", 10, 10);
    }
}
