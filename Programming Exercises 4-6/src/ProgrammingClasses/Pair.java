/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package ProgrammingClasses;

/**
 *
 * @author 114158
 */
public class Pair {
double first, second;
public Pair(double firstNumber, double secondNumber)
{
    first = firstNumber;
    second = secondNumber;
}
public double getSum() { return first + second; }
public double getDifference() { return first - second; }
public double getProduct() { return first * second; }
public double getAverage() { return (first + second) / 2; }
public double getDistance() { return Math.abs(getDifference()); }
public double getMaximum() {
if(first > second) { return first; }
return second;
}
public double getMinimum() {
    if(first < second) { return first; }
    return second;
}
@Override public String toString()
{
    return "Numbers: {" + first + ", " + second + "}";
}
}
