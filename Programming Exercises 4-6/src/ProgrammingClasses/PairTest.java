/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package ProgrammingClasses;
import ProgrammingClasses.Pair;

/**
 *
 * @author 114158
 */
public class PairTest {
    Pair pairObj;
public PairTest(double first, double second){
    pairObj = new Pair(first, second);
}
public void Evoke()
{
    System.out.println(pairObj.toString());
    System.out.println("Sum: " + pairObj.getSum());
    System.out.println("Difference: " + pairObj.getDifference());
    System.out.println("Product: " + pairObj.getProduct());
    System.out.println("Average: " + pairObj.getAverage());
    System.out.println("Distance: " + pairObj.getDistance());
    System.out.println("Maximum: " + pairObj.getMaximum());
    System.out.println("Minimum: " + pairObj.getMinimum());
}
}
