/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package ProgrammingClasses;
import java.awt.Point;

/**
 *
 * @author 114158
 */
public class Line {
    private Point pn1 = null;
    private double m = 0;
    private Point pn2 = null;
    private boolean lineSegment = false;
    
    public Line(Point startPnt, double slope) {
       this.pn1 = startPnt;
       this.m = slope;
       lineSegment = false;
    }
    public Line(Point point1, Point point2)    {
        this.pn1 = point1;
        this.pn2 = point2;
        this.m = (point1.y - point2.y) / (point1.x - point2.y);
        lineSegment = true;
    }
    public Line(double slope, Point yIntercept) {
        this.m = slope;
        this.pn1 = yIntercept;
        lineSegment = false;
    }
    public Line(double xEquals) {
        this.m = 0;
        this.pn1 = new Point((int)xEquals, 0);
        this.pn2 = new Point((int)xEquals, 10);
        lineSegment = false;
    }
    
    public boolean intersects(Line other)
    {
        //if(this.lineSegment || other.lineSegment)
        //{
        // you know, I'm far too lazy for this  
        //}
        if(this.m != other.m) { return true; } //well.. at some point :P
        return false;
    }
    public boolean equals(Line other)
    {
        if(this.lineSegment == other.lineSegment)
        {
            if(this.pn1.equals(other.pn1) && pn2.equals(other.pn2)) { return true; }
        } else {
            if(this.m == other.m && this.pn1.equals(other.pn1)) { return true; }
        }
        return false;
    }
    public boolean isParallel(Line other)
    {
        if(this.m == other.m) { return true; }
        return false;
    }
    
    @Override public String toString()
    {
        String ret = "";
        return ret;
    }
}
