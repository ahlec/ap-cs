/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package gridworldsimulations;
import info.gridworld.world.*;
import info.gridworld.gui.*;
import info.gridworld.grid.*;
import info.gridworld.actor.*;

/**
 *
 * @author 114158
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args)
    {
        ActorWorld world = new ActorWorld();
        world.add(world.getRandomEmptyLocation(), new Bug());
        world.show();
    }

}
