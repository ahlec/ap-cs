/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package programmingexercises46;
import ProgrammingClasses.CannonballFrame;
import ProgrammingClasses.PairTest;
import ProgrammingClasses.Cannonball;
import ProgrammingClasses.Line;
import java.awt.Point;

/**
 *
 * @author 114158
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    private static void printHeader(int chapter, int exercise)
    {
        System.out.println();
        System.out.println("Chapter " + chapter + " - Exercise " + exercise);
        System.out.println("<------------------->");
    }
    public static void main(String[] args) {
        System.out.println(18 / 4 + 18 % 4);
        printHeader(4, 4);
        PairTest testPair = new PairTest(10, 55);
        testPair.Evoke();
        printHeader(6, 18);
        Line ln1 = new Line(new Point(5, 9), new Point(0, 0));
        Line ln2 = new Line(4.3);
        System.out.println("Line One: " + ln1.toString());
        System.out.println("Line Two: " + ln2.toString());
        System.out.println("Intersects: " + ln1.intersects(ln2));
        System.out.println("Equals: " + ln1.equals(ln2));
        System.out.println("Is Parallel: " + ln1.isParallel(ln2));
        printHeader(7, 4);
        Cannonball projectile = new Cannonball();
        //projectile.fire(100);
        projectile.fireAtAngle(10, 60);
    }
}
