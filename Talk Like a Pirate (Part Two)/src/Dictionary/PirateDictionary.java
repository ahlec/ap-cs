/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package Dictionary;
import Dictionary.PartOfSpeech;
import java.util.Random;

/**
 *
 * @author Jacob
 */
public class PirateDictionary {

    private String[] engWords = new String[0];
    private String[] pirWords = new String[0];
    //private PartOfSpeech[] parOfSpeech = new PartOfSpeech[0];
    private Random rndGeneration = new Random();
    private long execTime = 0;
    
    public PirateDictionary()
    {
        this.addWord("Insane", "addled");
        this.addWord("After", "aft");
        this.addWord("Reward", "booty");
        this.addWord("mr", "cap'n");
        this.addWord("Mister", "cap'n");
        this.addWord("Pirate", "corsair");
        this.addWord("Friend", "bucko");
        this.addWord("Song", "chantey");
        this.addWord("Money", "dubloon");
        this.addWord("Coin", "dubloon");
        this.addWord("Food", "grub");
        this.addWord("Punish", "keelhaul");
        this.addWord("Boy", "lad");
        this.addWord("Girl", "lass");
        this.addWord("To Desert", "maroon");
        this.addWord("To Steal", "plunder");
        this.addWord("Sick", "poxed");
        this.addWord("Alcohol", "rum");
        this.addWord("Strange", "rum");
        this.addWord("Clean", "swab");
        this.addWord("Woman", "wench");
        this.addWord("Hi", "ahoy");
        this.addWord("Hello", "ahoy");
        this.addWord("Am", "be");
        this.addWord("Is", "be");
        this.addWord("Sword", "cutlass");
        this.addWord("Before", "fore");
        this.addWord("Cheat", "hornswaggle");
        this.addWord("Destroy", "pillage");
        this.addWord("My", "me");
        this.addWord("Are", "be");
        this.addWord("Yes", "aye");
        this.addWord("You", "ye");
        this.addWord("Your", "yer");
        this.addWord("Forward", "fore");
        this.addWord("Toilet", "head");
    }
    private void addWord(String english, String pirate)//, PartOfSpeech pos)
    {
        long beginMil = System.currentTimeMillis();
        String[] newEngWrd = new String[engWords.length + 1];
        String[] newPirWrd = new String[pirWords.length + 1];
        //PartOfSpeech[] newPOS = new PartOfSpeech[parOfSpeech.length + 1];
        System.arraycopy(engWords, 0, newEngWrd, 0, engWords.length);
        newEngWrd[engWords.length] = english;
        this.engWords = newEngWrd;
        System.arraycopy(pirWords, 0, newPirWrd, 0, pirWords.length);
        newPirWrd[pirWords.length] = pirate;
        this.pirWords = newPirWrd;
        //System.arraycopy(parOfSpeech, 0, newPOS, 0, parOfSpeech.length);
        //newPOS[parOfSpeech.length] = pos;
        //this.parOfSpeech = newPOS;
        this.execTime += System.currentTimeMillis() - beginMil;
    }
    
    /* Returns an English word for the given entry
     * @param index  An integer for referencing the pirate dictionary
     * @return  The English word for the given entry
     */
    public String getEnglishWord(int index) { return this.engWords[index]; }
    
    /* Returns a Pirate word for the given entry
     * @param index  An integer for referencing the pirate dictionary
     * @return  The Pirate word for the given entry
     */
    public String getPirateWord(int index) { return this.pirWords[index]; }
    
    /* Returns the Part of Speech for the given entry in the dictionary
     * @param index  An integer for referencing the pirate dictionary
     * @return  The PartOfSpeech for the given entry
     */
    //public PartOfSpeech getPartOfSpeech(int index) { return this.parOfSpeech[index]; }
    
    /* Returns the number of entries in the pirate dictionary
     * @return  The number of entries in the pirate dictionary
     */
    public int getNumberEntries() { return this.pirWords.length; }
    
    /* Returns the time it took to initialize and populate the pirate dictionary
     * @return  The double number entry of execution time (measured in milliseconds)
     */
    public long getExecutionTime() { return this.execTime; }
    
    public int searchForEnglish(String english)
    {
        for(int srch = 0; srch < this.engWords.length; srch++)
        {
            if(this.engWords[srch].equalsIgnoreCase(english)){
                return srch;
            }
        }
        return -1;
    }
    
    public String translateWord(String english)
    {
        String pirateReturn = "";
        String[] wordBreakdowns = tokenizerToStringArray(new java.util.StringTokenizer(english,
                ",;:", true));
        int wordIndex = getFirstWordIndex(wordBreakdowns);
        if(wordIndex < 0) { return english; }
        String actualWord = wordBreakdowns[wordIndex];
        boolean plural = (actualWord.charAt(actualWord.length() - 1) == 's'); // most likely plural
        if(plural) { actualWord = actualWord.substring(0, actualWord.length() - 1); }
        int englishIndex = this.searchForEnglish(actualWord);
        if(englishIndex == -1) { return english; }
        pirateReturn = this.getPirateWord(englishIndex);
        if(plural) { pirateReturn += "s"; }
        String prePunctuation = "";
        for(int prePunct = 0; prePunct < wordIndex; prePunct++)
        {
            prePunctuation += wordBreakdowns[prePunct];
        }
        pirateReturn = prePunctuation + capitalizeByExample(pirateReturn, english);
        for(int postPunct = wordIndex + 1; postPunct < wordBreakdowns.length; postPunct++)
        {
            pirateReturn += wordBreakdowns[postPunct];
        }
        return pirateReturn;
    }
    
    private String capitalizeByExample(String input, String example)
    {
        boolean[] capitalizedChar = new boolean[example.length()];
        for(int ripCapitalized = 0; ripCapitalized < capitalizedChar.length; ripCapitalized++) // sounds painful
        {
            if(Character.isUpperCase(example.charAt(ripCapitalized)))
                capitalizedChar[ripCapitalized] = true;
            else
                capitalizedChar[ripCapitalized] = false;
        }
        char[] retChars = new char[input.length()];
        for(int popChars = 0; popChars < retChars.length; popChars++)
        {
            retChars[popChars] = input.charAt(popChars);
        }
        for(int makeCapitalized = 0; makeCapitalized < retChars.length; makeCapitalized++)
        { 
            int findIndex = (int)Math.floor((makeCapitalized / (retChars.length * 1.0)) * example.length());
            if(capitalizedChar[findIndex])
                retChars[makeCapitalized] = Character.toUpperCase(retChars[makeCapitalized]);
        }
        String retString = "";
        for(int pieceBack = 0; pieceBack < retChars.length; pieceBack++)
        {
            retString += Character.toString(retChars[pieceBack]);
        }
        return retString;
    }
    public String endOfSentence()
    {
        if(this.rndGeneration.nextBoolean()) { return " Arr!"; }
        return "";
    }
    
    private String[] tokenizerToStringArray(java.util.StringTokenizer tokenizer)
    {
        String[] parsed = new String[0];
        while(tokenizer.hasMoreTokens())
        {
            String[] newParsed = new String[parsed.length + 1];
            System.arraycopy(parsed, 0, newParsed, 0, parsed.length);
            newParsed[parsed.length] = tokenizer.nextToken();
            parsed = newParsed;
        }
        return parsed;
    }
    private int getFirstWordIndex(String[] parts)
    {
        for(int search = 0; search < parts.length; search++)
        {
            if(parts[search].matches("[a-zA-Z0-9]*")) { return search; }
        }
        return -1;
    }
}