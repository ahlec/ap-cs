package JavaObjectsClasses;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author 114158
 */

public class SavingsAccount {
    private double interestRate = 0.0;
    private double balance = 0.0;
    public SavingsAccount(double initialBalance, double initialInterestRate)
    {
        this.balance = initialBalance;
        this.interestRate = (initialInterestRate * .01);
    }
    public void addInterest() { this.balance += (this.balance * this.interestRate); }
    public void withdraw(double amount) { this.balance -= amount; }
    public void deposit(double amount) { this.balance += amount; }
    public void setInterestRate(double rate) { this.interestRate = (rate * .1); }
    public double getBalance() { return this.balance; }
}
