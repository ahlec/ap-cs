/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.deitloff.buggies;
import java.awt.Color;

/**
 *
 * @author 114158
 * 
 * Moodybug -- the more bugs around him, the gloomier he gets... how sad?
 */
public class MoodyBug extends NeighbourBug
{
    Color _baseColor;
    public MoodyBug(Color baseColor)
    {
        //super();
        _baseColor = baseColor;
        setColor(baseColor);
    }
    
    @Override
    public void setColor(Color arg0) {
        super.setColor(arg0);
    }
    
    @Override
    public void act() {
        super.act();
        
        double multiplyVal = 1.0;
        if (countNeighbours() > 0)
            multiplyVal = (countNeighbours() / 8.0) * 3;
        if (multiplyVal > 1.0)
            multiplyVal = 1.0;
        if (multiplyVal > 0.0)
            setColor(new Color((int)(_baseColor.getRed() * multiplyVal),
                (int)(_baseColor.getGreen() * multiplyVal),
                (int)(_baseColor.getBlue() * multiplyVal) , _baseColor.getAlpha()));
    }
}
