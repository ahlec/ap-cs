/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package designingclasses;
import designingclasses.Geometry;

/**
 *
 * @author 114158
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        double radius = 5;
        double height = 3;
        System.out.println("(Sphere) Volume: " + Geometry.sphereVolume(radius));
        System.out.println("(Sphere) Surface Area: " + Geometry.sphereSurface(radius));
        System.out.println("(Cylinder) Volume: " + Geometry.cylinderVolume(radius, height));
        System.out.println("(Cylinder) Surface Area: " + Geometry.cylinderSurface(radius, height));
        System.out.println("(Cone) Volume: " + Geometry.coneVolume(radius, height));
        System.out.println("(Cone) Surface Area: " + Geometry.coneSurface(radius, height));
    }

}
