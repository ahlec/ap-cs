/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.deitloff.zoology;

/**
 *
 * @author 114158
 */
public class Bull implements Mammal
{
    public String whatAmI()
    {
        return "bull";
    }
    public String speak()
    {
        return "bellow";
    }
    public String getPic()
    {
        return "src/com/deitloff/zoology/pictureBull.png";
    }
}
