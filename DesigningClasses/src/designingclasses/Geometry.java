package designingclasses;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author 114158
 */
public class Geometry {
    public static double sphereVolume(double radius)
    {
        return (4 /3.0) * Math.PI * Math.pow(radius, 3);
    }
    public static double sphereSurface(double radius)
    {
        return 4 * Math.PI * Math.pow(radius, 2);
    }
    public static double coneVolume(double radius, double height)
    {
        return (1 / 3.0) * Math.PI * Math.pow(radius, 2) * height;
    }
    public static double coneSurface(double radius, double height)
    {
        return Math.PI * radius  * Math.sqrt(Math.pow(radius, 2) + Math.pow(height, 2));
    }
    public static double cylinderVolume(double radius, double height)
    {
        return Math.PI * Math.pow(radius, 2) * height;
    }
    public static double cylinderSurface(double radius, double height)
    {
        return (2 * Math.PI * Math.pow(radius, 2))  + (2 * Math.PI * radius * height);
    }
}
