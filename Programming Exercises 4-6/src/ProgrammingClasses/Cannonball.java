/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package ProgrammingClasses;
import java.awt.Graphics;

/**
 *
 * @author 114158
 */
public class Cannonball {
    final double deltaT = 0.01;
    final double gravitation = 9.81;
    public void fire(double initialVelocity)
    {
        double curVelocity = initialVelocity;
        double curPosition = 0;
        int secondsElapsed = 0;
        do{
            for(int milliSeconds = 0; milliSeconds < 100; milliSeconds++)
            {
                curVelocity -= gravitation * deltaT;
                curPosition += curVelocity * deltaT;
            }
            secondsElapsed++;
            System.out.print("Second " + secondsElapsed + ": ");
            System.out.print("[Position: " + curPosition + "] ");
            System.out.print("[Velocity: " + curVelocity + "] ");
            System.out.println("[Exact: " + 
                (-0.5 * gravitation * secondsElapsed * secondsElapsed 
                + initialVelocity * secondsElapsed)
                    + "]");
        }while(curPosition > 0);
    }
    
    CannonballFrame cannonFrame = new CannonballFrame();
    public void fireAtAngle(double initialVelocity, double angle)
    {
        double angleRads = Math.toRadians(angle);
        /*
        String angRadsStr = Double.toString(angleRads);
        int endOfTrunc = angRadsStr.indexOf(".");
        if(endOfTrunc < 0)
            endOfTrunc = angRadsStr.length();
        else
            endOfTrunc += 2;
        if(endOfTrunc > angRadsStr.length()) { endOfTrunc = angRadsStr.length(); }
        angleRads = Double.parseDouble(angRadsStr.substring(0, endOfTrunc));
        */
        cannonFrame.setSimulationValues(initialVelocity, angle, angleRads);
        cannonFrame.setTitle("Cannonball Simulation");
        cannonFrame.setVisible(true);
        cannonFrame.beginSimulation();
    }
}
