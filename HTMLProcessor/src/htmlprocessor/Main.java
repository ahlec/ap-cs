/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package htmlprocessor;
import htmlprocessor.HTMLProcessor;

/**
 * @author 114158
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        String htmlCode = "<div style='border-bottom:1px solid black;'><font color='blue'><b>hello</b> <u>world!</u></font></div>";
        
        System.out.println("#   Source Code: " + htmlCode);
        String firstTag = HTMLProcessor.findFirstTag(htmlCode);
        System.out.println("#   First Tag: " + firstTag);
        System.out.println("#   Without '" + firstTag + "': " +
                HTMLProcessor.removeTag(htmlCode, firstTag));
        System.out.println("#   Without tags: " + HTMLProcessor.removeAllTags(htmlCode));
         }

}
