/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package exercisesp1;

/**
 *
 * @author 114158
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void printBoxRow(int boxesOnRow, boolean hasBottom, int boxOffset)
    {
        for(int doOffsetTop = 0; doOffsetTop < boxOffset; doOffsetTop++) { System.out.print("    "); }
        for(int topRow = 0; topRow < boxesOnRow; topRow++)
        { System.out.print("+---");}
        System.out.println("+");
        for(int doOffsetMid = 0; doOffsetMid < boxOffset; doOffsetMid++) { System.out.print("    "); }
        for(int heightRow = 0; heightRow < boxesOnRow; heightRow++)
        { System.out.print("|   "); }
        System.out.println("|");
        if(hasBottom)
        {
            for(int doOffsetBtm = 0; doOffsetBtm < boxOffset; doOffsetBtm++) { System.out.print("    "); }
            for(int bottomRow = 0; bottomRow < boxesOnRow; bottomRow++)
            { System.out.print("+---"); }
            System.out.println("+");
        }
    }
    public static void printExerciseHeader(int chapter, int number)
    {
        System.out.println();
        System.out.println("<----------------------->");
        System.out.println("Exercise P" + chapter + "." + number);
        //System.out.println();
    }
    public static void main(String[] args) {
        // Exercise P1.3
        printExerciseHeader(1, 3);
        printBoxRow(3, false, 0);
        printBoxRow(3, false, 0);
        printBoxRow(3, true, 0);
        // Exercise P1.4
        printExerciseHeader(1, 4);
        printBoxRow(1, false, 3);
        printBoxRow(2, false, 2);
        printBoxRow(3, false, 1);
        printBoxRow(4, true, 0);
        // Exercise P1.5
        printExerciseHeader(1, 5);
        int totalSum = 0;
        for(int printSums = 1; printSums <= 10; printSums++)
        {
            System.out.print(printSums);
            totalSum += printSums;
            if(printSums < 10) { System.out.print(" + "); } else { System.out.print(" = "); }
        }
        System.out.print(totalSum);
        // Exercise P1.6
        printExerciseHeader(1, 6);
        int denomNums = 10;
        float denomSum = 0;
        for(float calculate = 1; calculate <= denomNums; calculate++)
        {
            denomSum += 1 / calculate;
        }
        System.out.println("Sum:" + denomSum);
        // Exercise P1.7
        printExerciseHeader(1, 7);
        DialogTester dialogTester = new DialogTester();
        dialogTester.hello("Hello, Jacob!", false);
        // Exercise P1.8
        printExerciseHeader(1, 8);
        dialogTester.input("What is your name?", true);
        }
    }
