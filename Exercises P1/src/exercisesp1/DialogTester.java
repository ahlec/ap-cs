/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package exercisesp1;

/**
 *
 * @author 114158
 */
import javax.swing.JOptionPane;
public class DialogTester {
    public static void hello(String strPrompt, boolean exitProgram)
    {
        JOptionPane.showMessageDialog(null, strPrompt);
        if(exitProgram) { System.exit(0); }
    }
    public static void input(String strPrompt, boolean exitProgram)
    {
        String name = JOptionPane.showInputDialog(strPrompt);
        System.out.println("Hello, " + name + "!");
        if(exitProgram) { System.exit(0); }
    }
}
