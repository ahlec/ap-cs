/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package arrayarraylistexercises;

/**
 *
 * @author 114158
 */
public class Square {

    int[][] square_values = new int[0][0];
    String excessValues = "";
    int nDimension = 0;
    boolean arbitrarilyDead = false;
    
    public Square(int dimension)
    {
        nDimension = dimension;
        square_values = new int[dimension][dimension];
    }
    
    public void add(int nextNumber)
    {
        for (int colTier = 0; colTier < nDimension; colTier++)
            for (int rowTier = 0; rowTier < nDimension; rowTier++)
            {
                if (square_values[rowTier][colTier] > 0)
                    continue;
                square_values[rowTier][colTier] = nextNumber;
                return;
            }
        excessValues += Integer.toString(nextNumber) + "|";
        arbitrarilyDead = true;
    }
    
    public boolean isMagic()
    {
        // Is it even a square?
        if (arbitrarilyDead)
            return false;
        
        // Do we have any repeat numbers, any values outside of the magic square bounds, or
        //    any values that are equal to 0 (not set)?
        boolean[] used_values = new boolean[nDimension * nDimension];
        for (int set1 = 0; set1 < nDimension; set1++)
            for (int set2 = 0; set2 < nDimension; set2++)
            {
                if (square_values[set1][set2] <= 0)
                    return false; // The number is less than or equal to 0!
                if (square_values[set1][set2] > used_values.length)
                    return false; // The number is greater than the number of positions available,
                                // which means that at least one number was skipped
                if (used_values[square_values[set1][set2] - 1])
                    return false; // That index, which is 1 less than the number inside that
                                // position, has already been reached
                used_values[square_values[set1][set2] - 1] = true;
            }
        
        // Now, is it a magic square?
        int[] rowSums = new int[nDimension];
        int[] colSums = new int[nDimension];
        int diagLR = 0; // Diagonal '\'
        int diagRL = 0; // Diagonal '/'
        for (int tier1 = 0; tier1 < nDimension; tier1++)
            for (int tier2 = 0; tier2 < nDimension; tier2++)
            {
                rowSums[tier1] += square_values[tier1][tier2];
                colSums[tier1] += square_values[tier1][tier2];
                if (tier1 == tier2)
                    diagLR += square_values[tier1][tier2];
                if (tier1 + tier2 == square_values.length - 1)
                    diagRL += square_values[tier1][tier2];
            }
        for (int check = 1; check < nDimension; check++)
        {
            if (rowSums[check] != rowSums[0] || rowSums[check] != colSums[0])
                return false;
            if (colSums[check] != colSums[0] || colSums[check] != rowSums[0])
                return false;
        }
        if (diagLR != diagRL || diagLR != colSums[0] || diagRL != colSums[0])
            return false;
        return true;
    }

    @Override
    public String toString() {
        String returnValue = "";
        for (int colsOut = 0; colsOut < nDimension; colsOut++)
        {
            for (int rowsOut = 0; rowsOut < nDimension; rowsOut++)
            {
                if (square_values[rowsOut][colsOut] < 10)
                    returnValue += " ";
                returnValue += Integer.toString(square_values[rowsOut][colsOut]);
                if (rowsOut < nDimension - 1)
                    returnValue += " | ";
            }
            if (colsOut < nDimension -1)
                returnValue += "\n";
        }
        // Output excess values!
        int excessValuesGoneOut = 0;
        String[] excessValuesSplit = excessValues.split("\\|");
        for (int excess = 0; excess < excessValuesSplit.length; excess++)
        {
            if (excess == 0)
                returnValue += "\n";
            if (excessValuesSplit[excess].length() < 2)
                returnValue += " ";
            returnValue += excessValuesSplit[excess];
            if (excess == nDimension - 1)
                returnValue += "\n";
            else
                returnValue += " | ";
        }
        return returnValue;
    }
}
