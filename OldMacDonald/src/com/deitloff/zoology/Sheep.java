/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.deitloff.zoology;

/**
 *
 * @author 114158
 */
public class Sheep implements Mammal
{
    public String getPic()
    {
        return "src/com/deitloff/zoology/pictureSheep.png";
    }
    public String speak()
    {
        return "baa";
    }
    public String whatAmI()
    {
        return "sheep";
    }
}
