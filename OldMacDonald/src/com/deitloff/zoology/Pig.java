/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.deitloff.zoology;

/**
 *
 * @author 114158
 */
public class Pig implements Mammal
{
    public String getPic()
    {
        return "src/com/deitloff/zoology/picturePig.png";
    }
    public String whatAmI()
    {
        return "pig";
    }
    public String speak()
    {
        return "oink";
    }
}
