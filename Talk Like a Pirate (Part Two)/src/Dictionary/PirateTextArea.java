/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package Dictionary;

import java.awt.Graphics;
import java.awt.Image;

/**
 * @author 114158
 */
public class PirateTextArea extends javax.swing.JTextArea{

    public Image backgroundImage = null;
    
    public PirateTextArea() {
        this.setOpaque(false);
        this.setDoubleBuffered(true);
        this.setWrapStyleWord(true);
        this.setLineWrap(true);
        this.setMargin(new java.awt.Insets(4,4,4,4));
        this.setBorder(new javax.swing.border.EmptyBorder(1,1,1,1));//LineBorder(java.awt.Color.BLACK));
    }
    
    public void setBackgroundImage(Image img) { this.backgroundImage = img; }
    
    @Override
    protected void paintComponent(Graphics g) {
        if(backgroundImage != null)
            g.drawImage(backgroundImage, this.getVisibleRect().x, this.getVisibleRect().y, null);
//            g.drawImage(backgroundImage, 0, g.getClipBounds().y, null);
        super.paintComponent(g);
    }
    
}
