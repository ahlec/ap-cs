/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package JavaObjectsClasses;

/**
 *
 * @author 114158
 */
public class Car {
private double mpg = 0;
private double gasLevel = 0;
public Car(double milesPerGallon) { this.mpg = milesPerGallon; }
public double getGas() { return this.gasLevel; }
public void addGas(double amount) { this.gasLevel += amount; }
public void drive(double miles) { this.gasLevel -= (miles / this.mpg); }
}
