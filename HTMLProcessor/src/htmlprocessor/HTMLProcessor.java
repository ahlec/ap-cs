/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package htmlprocessor;
import java.util.ArrayList;

/**
 *
 * @author 114158
 */
public class HTMLProcessor {

    /* we are aware that certain tags carry parameters
     *    (such examples would be <a>, <div>, <table> -- just about anything with *style* XD
     *  tags that might open with <a href='index.php'> close with just </a>
     * so, the actual tag is whatever is after the opening symbol, until the first space
     * 
     * let's do this ;P     */
    
    public static String findFirstTag(String code) // note -- returns WITH brackets intact
    {
        if (code.indexOf('<') < 0 || code.indexOf('>') < 0)
            return null;

        int startIndex = -1;
        while (true)
        {
            int beforeStartIndex = startIndex;
            if (code.indexOf("</", startIndex) == -1)
            {
                startIndex = code.indexOf("<", startIndex);
                break;
            }
            else if (code.indexOf("<", startIndex) != code.indexOf("</", startIndex))
            {
                startIndex = code.indexOf("<", startIndex);
                break;
            }
            startIndex = code.indexOf("<", startIndex + 1);
            if (beforeStartIndex == startIndex)
                return null;
        }
        int spaceEnd = code.indexOf(" ", startIndex);
        int bracketEnd = code.indexOf(">", startIndex);
        int endIndex = 0;
        if (spaceEnd == -1)
            endIndex = bracketEnd;
        else
            endIndex = (bracketEnd > spaceEnd ? spaceEnd : bracketEnd);
        String tagCode = stripBrackets(code.substring(startIndex, endIndex + 1));
        return addBrackets(tagCode);
    }
    
    public static String stripBrackets(String tag)
    {
        assert (!tag.equals(null) || tag.length() > 2);
        String noBrackets = tag;
        if (noBrackets.startsWith("<"))
            noBrackets = noBrackets.substring(1);
        if (noBrackets.startsWith("/"))
            noBrackets = noBrackets.substring(1);
        if (noBrackets.endsWith(">"))
            noBrackets = noBrackets.substring(0, noBrackets.length() - 1);
        if (noBrackets.endsWith(" "))
            noBrackets = noBrackets.substring(0, noBrackets.length() - 1);
        return noBrackets;
    }
    public static String addBrackets(String tag)
    {
        return addBrackets(tag, false, false);
    }
    public static String addBrackets(String tag, boolean isClosingTag)
    {
        return addBrackets(tag, isClosingTag, false);
    }
    public static String addBrackets(String tag, boolean isClosingTag, boolean endsWithSpace)
    {
        assert (!tag.equals(null) || tag.length() > 0);
        String withBrackets = stripBrackets(tag);
        if (isClosingTag)
            withBrackets = "</" + withBrackets;
        else
            withBrackets = "<" + withBrackets;
        if (endsWithSpace)
            withBrackets += " ";
        else
            withBrackets += ">";
        return withBrackets;
    }
    
    public static String removeTag(String code, String tag)
    {
        String sansTagCode = code;
        sansTagCode = removeTagSrc(sansTagCode, tag, false);
        if (hasClosingTag(sansTagCode, tag))
            sansTagCode = removeTagSrc(sansTagCode, tag, true);
        return sansTagCode;
    }
    private static String removeTagSrc(String code, String tag, boolean isClosing)
    {
        assert (!code.equals(null) || code.length() > 0);
        assert (!tag.equals(null) || tag.length() > 0);
        String tagCodeShort = addBrackets(tag, isClosing, false);
        String tagCodeSpace = addBrackets(tag, isClosing, true);
        if (code.indexOf(tagCodeShort) < 0 && code.indexOf(tagCodeSpace) < 0)
            return code;
        
        boolean usesShortTag = (code.indexOf(tagCodeShort) >= 0);
        
        int tagCodeLocation = (usesShortTag ? code.indexOf(tagCodeShort) :
            code.indexOf(tagCodeSpace));
        
        int endIndex = code.indexOf(">", tagCodeLocation);
        
        int preCodeEndIndex = endIndex - (endIndex - tagCodeLocation);

        assert (preCodeEndIndex > -1);
        assert (endIndex > -1);
        return code.substring(0, preCodeEndIndex) + code.substring(endIndex + 1);
    }
    public static String removeAllTags(String code)
    {
        assert (!code.equals(null) || code.length() > 0);
        if (code.indexOf("<") < 0 && code.indexOf(">") < 0)
            return code;
        
        String workCode = code;
        while (findFirstTag(workCode) != null)
        {
            String curTag = stripBrackets(findFirstTag(workCode));
            if (!hasClosingTag(workCode, curTag))
                throw new IllegalArgumentException();
            workCode = removeTag(workCode, curTag);
        }
        return workCode;
    }
    public static boolean hasClosingTag(String code, String tag)
    {
        if (code.indexOf(addBrackets(tag, true)) == -1)
            return false;
        return true;
    }
}
