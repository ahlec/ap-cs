/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.deitloff.zoology;

/**
 *
 * @author 114158
 */
public class Cow implements Mammal
{
    public String getPic()
    {
        return "src/com/deitloff/zoology/pictureCow.png";
    }
    public String speak()
    {
        return "moo";
    }
    public String whatAmI()
    {
        return "cow";
    }
}
