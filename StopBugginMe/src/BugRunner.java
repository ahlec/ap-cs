/* 
 * AP(r) Computer Science GridWorld Case Study:
 * Copyright(c) 2005-2006 Cay S. Horstmann (http://horstmann.com)
 *
 * This code is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation.
 *
 * This code is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * @author Cay Horstmann
 */

import info.gridworld.actor.ActorWorld;
import info.gridworld.actor.Bug;
import info.gridworld.actor.Rock;
import info.gridworld.grid.Location;

/**
 * This class runs a world that contains a bug and a rock, added at random
 * locations. Click on empty locations to add additional actors. Click on
 * populated locations to invoke methods on their occupants. <br />
 * To build your own worlds, define your own actors and a runner class. See the
 * BoxBugRunner (in the boxBug folder) for an example. <br />
 * This class is not tested on the AP CS A and AB exams.
 */
public class BugRunner
{
    public static void main(String[] args)
    {
        //info.gridworld.grid.Grid _nameGrid = new info.gridworld.grid.
        //info.gridworld.grid.Grid _nameGrid = new info.gridworld.grid.BoundedGrid(25, 25);
        //ActorWorld _world = new ActorWorld(_nameGrid);
        ActorWorld _world = new ActorWorld();
        
        //_world.add(new com.deitloff.buggies.ButtBug());
        _world.add(new com.deitloff.buggies.TunnelBug());
        for (int bugs = 0; bugs < 7; bugs++)
            _world.add(new com.deitloff.buggies.MoodyBug(
                    new java.awt.Color((int)(Math.random() * 255), 
                    (int)(Math.random() * 255), (int)(Math.random() * 255), 255)));
        //_world.add(new com.deitloff.buggies.ParanoidBug());
        for (int rocks = 0; rocks < 20; rocks++)
            _world.add(new info.gridworld.actor.Rock());
        
        /*// J
        java.util.ArrayList<Location> _rocks = new java.util.ArrayList<Location>();
        _rocks.add(new Location(0,2));
        _rocks.add(new Location(0,3));
        _rocks.add(new Location(1,1));
        _rocks.add(new Location(1,12));
        _rocks.add(new Location(2,12));
        _rocks.add(new Location(3,12));
        _rocks.add(new Location(4,12));
        _rocks.add(new Location(3,7));
        _rocks.add(new Location(2,6));
        _rocks.add(new Location(2,8));
        _rocks.add(new Location(2,9));
        _rocks.add(new Location(3,9));
        _rocks.add(new Location(5,11));
        _rocks.add(new Location(5,10));
        _rocks.add(new Location(4,6));
        _rocks.add(new Location(5,6));
        _rocks.add(new Location(14,9));
        _rocks.add(new Location(15,8));
        _rocks.add(new Location(15,7));
        _rocks.add(new Location(14,0));
        _rocks.add(new Location(5,0));
        _rocks.add(new Location(5,1));
        _rocks.add(new Location(6,4));
        _rocks.add(new Location(7,4));
        _rocks.add(new Location(9,2));
        _rocks.add(new Location(11,2));
        _rocks.add(new Location(11,3));
        _rocks.add(new Location(10,6));
        _rocks.add(new Location(9,6));
        _rocks.add(new Location(10,1));
        _rocks.add(new Location(9,1));
        _rocks.add(new Location(8,4));
        _rocks.add(new Location(9,4));
        _rocks.add(new Location(11,4));
        _rocks.add(new Location(12,4));
        _rocks.add(new Location(13,5));
        _rocks.add(new Location(12,6));
        _rocks.add(new Location(13,6));
        _rocks.add(new Location(13,4));
        _rocks.add(new Location(2,5));
        _rocks.add(new Location(2,4));
        _rocks.add(new Location(3,2));
        _rocks.add(new Location(2,1));
        _rocks.add(new Location(2,7));
        _rocks.add(new Location(4,7));
        _rocks.add(new Location(5,4));
        _rocks.add(new Location(4,3));
        
        for (Location rock : _rocks)
            _world.add(rock, new Rock());
        Bug _j1 = new Bug();
        Bug _j2 = new Bug();
        _j1.setDirection(90);
        _j2.setDirection(135);
        _world.add(new Location(1,2), _j1);
        _world.add(new Location(3,8), _j2);
        */
        _world.show();
    }
}
